const { test, expect } = require('@playwright/test');

test('basic test', async ({ page }) => {
    await page.goto('http://nicasourcetesting.kinsta.cloud/');
    const title = page.locator('.site-title a');
    await expect(title).toHaveText('NicaSource Testing');
});